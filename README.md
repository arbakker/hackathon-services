# Hackathon services
For the hackathon during the Common Ground fieldlab on 25th and 26th of September 2019 we would like to expose some services on the NLX demo network so users are able to work with NLX. We offer the following services:

- [BRP API, Haal Centraal](https://github.com/VNG-Realisatie/Haal-Centraal-BRP-bevragen)
- [BAG API, Kadaster](https://github.com/lvbag/BAG-API/tree/master/Documentatie)
- [PDOK API, Kadaster](https://www.pdok.nl/next)

For more information about the specific configuration, check out the [service-config.toml](helm/values.yaml#L12).

## Installation
This repository contains a Helm chart with installation. The installation can be installed manually with:

```bash
helm upgrade --install hackathon-services ./helm \
    --namespace hackathon-services \
    --set "selfAddress=$SELF_ADDRESS" \
    --set "root_crt=$ROOT_CERT" \
    --set "org_crt=$ORG_CERT" \
    --set "org_key=$ORG_KEY"
```
